<?php

return [

    'model_class' => \Eurofirany\BaselinkerConnector\Models\BaselinkerChannel::class,

    /** Queue Settings */
    'queue' => [
        'enabled' => false, // Is queue for connector enabled
        'max_tries' => 3, // Max tries for failed job
        'max_running_jobs' => 5, // Max running jobs at the same time
        'max_wait_seconds' => 300, // Wait seconds when there are too many jobs
        'sleep_seconds' => 10, // Seconds to wait between attempts
        'max_per_minute' => 100 // Max jobs run per minute, 0 for no limit
    ]
];
