<?php

namespace Eurofirany\BaselinkerConnector;

use Eurofirany\BaselinkerConnector\Models\BaselinkerChannel;
use Eurofirany\ConnectorsQueue\ConnectorsQueue;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Cache;

class Api
{
    /** @var Client */
    private Client $client;

    /** @var BaselinkerChannel */
    private BaselinkerChannel $baselinkerChannel;

    private ConnectorsQueue $connectorsQueue;

    const BASELINKER_API_LINK = 'https://api.baselinker.com/connector.php';

    /**
     * Api constructor.
     * @param BaselinkerChannel $baselinkerChannel
     */
    public function __construct(BaselinkerChannel $baselinkerChannel)
    {
        // Create client instance
        $this->client = new Client();

        // Get access data from db
        $this->baselinkerChannel = $baselinkerChannel;

        $this->connectorsQueue = new ConnectorsQueue();

        $this->connectorsQueue->setting(
            config('bl.queue.enabled', false),
            config('bl.queue.max_tries', 3),
            config('bl.queue.max_running_jobs', 5),
            config('bl.queue.max_wait_seconds', 300),
            config('bl.queue.sleep_seconds', 10),
            config('bl.queue.max_per_minute', 100)
        );
    }

    /**
     * @param mixed $method
     * @param array $parameters
     * @param int $ttl
     * @param bool $ignoreCache
     * @return array
     * @throws GuzzleException
     * @throws \Exception
     */
    public function request(
        mixed $method,
        array $parameters = [],
        int   $ttl = 60,
        bool  $ignoreCache = false
    ): array
    {
        return $this->connectorsQueue->call(function () use ($method, $parameters, $ttl, $ignoreCache) {
            // Generate key
            $key = md5($method . serialize($parameters) . $this->baselinkerChannel->name);

            // Check for cache
            if (Cache::has($key) && $ignoreCache === false)
                return Cache::get($key);

            $response = json_decode($this->client->request('POST', self::BASELINKER_API_LINK, [
                'form_params' => [
                    'token' => $this->baselinkerChannel->token,
                    'method' => $method,
                    'parameters' => json_encode(['storage_id' => $this->baselinkerChannel->storage_id] + $parameters)
                ]
            ])->getBody(), true);

            Cache::put($key, $response, $ttl);

            return $response;
        });
    }

    /**
     * Set if you want or not to ignore queue for next requests
     * @param bool $ignoreQueueForRequests
     */
    public function setIgnoreQueueForRequests(bool $ignoreQueueForRequests)
    {
        $this->connectorsQueue->setWithQueue(!$ignoreQueueForRequests);
    }
}
