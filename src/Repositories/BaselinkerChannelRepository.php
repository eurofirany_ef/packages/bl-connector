<?php

namespace Eurofirany\BaselinkerConnector\Repositories;

use Eurofirany\BaselinkerConnector\Models\BaselinkerChannel;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BaselinkerChannelRepository
 * @property BaselinkerChannel model
 */
class BaselinkerChannelRepository
{
    /**
     * Init repository
     */
    public function __construct()
    {
        $this->model = new (config('bl.model_class', BaselinkerChannel::class))();
    }

    /**
     * Get all Baselinker Channels
     * @return BaselinkerChannel[]|Collection
     */
    public function index(): Collection|array
    {
        return $this->model->all();
    }

    /**
     * Store new Baselinker Channel
     * @param array $data Data of channel to be saved
     * @return BaselinkerChannel
     */
    public function store(array $data): BaselinkerChannel
    {
        return $this->model->create($data);
    }

    /**
     * Update Baselinker Channel data
     * @param BaselinkerChannel $baselinkerChannel Baselinker Channel to be updated
     * @param array $data New data for channel
     * @return bool|BaselinkerChannel
     */
    public function update(BaselinkerChannel $baselinkerChannel, array $data): bool|BaselinkerChannel
    {
        return $baselinkerChannel->update($data);
    }

    /**
     * Delete Baselinker Channel
     * @param BaselinkerChannel $baselinkerChannel Channel to be deleted
     * @return bool|null
     * @throws Exception
     */
    public function delete(BaselinkerChannel $baselinkerChannel): ?bool
    {
        return $baselinkerChannel->delete();
    }

    /**
     * Get Channel by ID, if not exists returns null
     * @param int|null $id ID for Channel to be taken
     * @return BaselinkerChannel|null
     */
    public function find(?int $id): ?BaselinkerChannel
    {
        return $this->model->find($id);
    }

    /**
     * Get Baselinker Channel by name. If not exists returns null
     * @param string $name Name of channel to be taken
     * @return Model|BaselinkerChannel|null
     */
    public function getByName(string $name): Model|BaselinkerChannel|null
    {
        return $this->model->where('name', $name)->first();
    }
}
