<?php

namespace Eurofirany\BaselinkerConnector\Responses;

use Eurofirany\CastToClass\CanCast;

/**
 * @property string status
 * @property string error_message
 * Class BaseResponse
 * @package Eurofirany\BaselinkerConnector\Responses
 */
class BaseResponse extends CanCast {}