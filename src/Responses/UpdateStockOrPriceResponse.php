<?php

namespace Eurofirany\BaselinkerConnector\Responses;

/**
 * @property int counter
 * @property string[] warnings
 * Class UpdateProductsQuantityResponse
 * @package Eurofirany\BaselinkerConnector\Responses
 */
class UpdateStockOrPriceResponse extends BaseResponse {}