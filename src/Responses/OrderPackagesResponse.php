<?php

namespace Eurofirany\BaselinkerConnector\Responses;

use Eurofirany\BaselinkerConnector\Responses\Schemes\OrderPackageScheme;
use Illuminate\Support\Collection;

/**
 * @property OrderPackageScheme[]|Collection packages
 * Class OrderPackagesResponse
 * @package Eurofirany\BaselinkerConnector\Responses
 */
class OrderPackagesResponse extends BaseResponse {
    protected array $map = ['packages' => OrderPackageScheme::class];
}
