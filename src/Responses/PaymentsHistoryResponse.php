<?php

namespace Eurofirany\BaselinkerConnector\Responses;

use Eurofirany\BaselinkerConnector\Responses\Schemes\PaymentScheme;
use Illuminate\Support\Collection;

/**
 * @property PaymentScheme[]|Collection payments
 * Class PaymentsHistoryResponse
 * @package Eurofirany\BaselinkerConnector\Responses
 */
class PaymentsHistoryResponse extends BaseResponse {
    protected array $map = ['payments' => PaymentScheme::class];
}