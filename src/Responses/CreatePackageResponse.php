<?php

namespace Eurofirany\BaselinkerConnector\Responses;

/**
 * @property int package_id
 * @property string package_number
 * Class CreatePackageResponse
 * @package Eurofirany\BaselinkerConnector\Responses
 */
class CreatePackageResponse extends BaseResponse {}