<?php

namespace Eurofirany\BaselinkerConnector\Responses;

/**
 * @property string storage_id
 * @property string product_id
 * @property string[] warnings
 * Class AddProductResponse
 * @package Eurofirany\BaselinkerConnector\Responses
 */
class AddProductResponse extends BaseResponse {}