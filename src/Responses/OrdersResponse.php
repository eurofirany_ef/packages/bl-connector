<?php

namespace Eurofirany\BaselinkerConnector\Responses;

use Eurofirany\BaselinkerConnector\Responses\Schemes\OrderScheme;
use Illuminate\Support\Collection;

/**
 * @property OrderScheme[]|Collection orders
 * Class OrdersResponse
 * @package Eurofirany\BaselinkerConnector\Responses
 */
class OrdersResponse extends BaseResponse {
    protected array $map = ['orders' => OrderScheme::class];
}