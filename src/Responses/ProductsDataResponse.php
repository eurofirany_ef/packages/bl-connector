<?php


namespace Eurofirany\BaselinkerConnector\Responses;

use Eurofirany\BaselinkerConnector\Responses\Schemes\ProductDataScheme;
use Illuminate\Support\Collection;

/**
 * @property string storage_id
 * @property ProductDataScheme[]|Collection products
 * Class ProductsDataResponse
 * @package Eurofirany\BaselinkerConnector\Responses
 */
class ProductsDataResponse extends BaseResponse {
    protected array $map = ['products' => ProductDataScheme::class];
}