<?php

namespace Eurofirany\BaselinkerConnector\Responses;

use Eurofirany\BaselinkerConnector\Responses\Schemes\ProductScheme;
use Illuminate\Support\Collection;

/**
 * @property string storage_id
 * @property ProductScheme[]|Collection products
 * Class ProductsListResponse
 * @package Eurofirany\BaselinkerConnector\Responses
 */
class ProductsListResponse extends BaseResponse {
    protected array $map = ['products' => ProductScheme::class];
}