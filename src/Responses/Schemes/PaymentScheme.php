<?php

namespace Eurofirany\BaselinkerConnector\Responses\Schemes;

use Eurofirany\CastToClass\CanCast;

/**
 * @property float paid_before
 * @property float paid_after
 * @property float total_price
 * @property string currency
 * @property string external_payment_id
 * @property int date
 * Class PaymentScheme
 * @package Eurofirany\BaselinkerConnector\Responses\Schemes
 */
class PaymentScheme extends CanCast {}