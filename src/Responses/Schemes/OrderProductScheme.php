<?php

namespace Eurofirany\BaselinkerConnector\Responses\Schemes;

/**
 * @property string storage
 * @property int storage_id
 * @property int order_product_id
 * @property string variant_id
 * @property string auction_id
 * @property string attributes
 * @property int tax_rate
 * @property float weight
 *
 * Class OrderProductScheme
 * @package Eurofirany\BaselinkerConnector\Responses\Schemes
 */
class OrderProductScheme extends ProductScheme {}