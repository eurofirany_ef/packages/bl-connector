<?php

namespace Eurofirany\BaselinkerConnector\Responses\Schemes;

use Eurofirany\CastToClass\CanCast;

/**
 * @property string storage_id
 * @property string name
 * @property string[] methods
 * Class StorageScheme
 * @package Eurofirany\BaselinkerConnector\Responses\Schemes
 */
class StorageScheme extends CanCast {}