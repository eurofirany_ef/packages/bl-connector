<?php


namespace Eurofirany\BaselinkerConnector\Responses\Schemes;

use Illuminate\Support\Collection;

/**
 * @property float price_netto
 * @property float price_wholesale_netto
 * @property float weight
 * @property string description
 * @property string description_extra1
 * @property string description_extra2
 * @property string description_extra3
 * @property string description_extra4
 * @property string man_name
 * @property string man_image
 * @property int category_id
 * @property string[] images
 * @property array[] features
 * @property ProductVariantScheme[]|Collection variants
 * Class ProductDataScheme
 * @package Eurofirany\BaselinkerConnector\Responses\Schemes
 */
class ProductDataScheme extends ProductScheme {
    protected array $map = ['variants' => ProductVariantScheme::class];
}