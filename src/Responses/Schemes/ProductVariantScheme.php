<?php

namespace Eurofirany\BaselinkerConnector\Responses\Schemes;

use Eurofirany\CastToClass\CanCast;

/**
 * @property int variant_id
 * @property string name
 * @property float price
 * @property int quantity
 * @property string ean
 * Class ProductVariantScheme
 * @package Eurofirany\BaselinkerConnector\Responses\Schemes
 */
class ProductVariantScheme extends CanCast {}