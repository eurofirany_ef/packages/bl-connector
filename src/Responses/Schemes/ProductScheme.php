<?php

namespace Eurofirany\BaselinkerConnector\Responses\Schemes;

use Eurofirany\CastToClass\CanCast;

/**
 * @property string product_id
 * @property string ean
 * @property string sku
 * @property string name
 * @property int quantity
 * @property float price_brutto
 * Class ProductScheme
 * @package Eurofirany\BaselinkerConnector\Responses\Schemes
 */
class ProductScheme extends CanCast {}