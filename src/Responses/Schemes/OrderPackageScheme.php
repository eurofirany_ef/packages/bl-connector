<?php

namespace Eurofirany\BaselinkerConnector\Responses\Schemes;

use Eurofirany\CastToClass\CanCast;

/**
 * @property int package_id
 * @property string courier_package_nr
 * @property string courier_inner_number
 * @property string courier_code
 * @property string courier_other_name
 * @property int tracking_status_date
 * @property int tracking_delivery_days
 * @property int tracking_status
 * Class OrderPackageScheme
 * @package Eurofirany\BaselinkerConnector\Responses\Schemes
 */
class OrderPackageScheme extends CanCast {}