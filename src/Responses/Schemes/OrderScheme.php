<?php


namespace Eurofirany\BaselinkerConnector\Responses\Schemes;

use Eurofirany\CastToClass\CanCast;
use Illuminate\Support\Collection;

/**
 * @property int order_id
 * @property int shop_order_id
 * @property string external_order_id
 * @property string order_source
 * @property int order_source_id
 * @property string order_source_info
 * @property int order_status_id
 * @property int date_add
 * @property int date_confirmed
 * @property int date_in_status
 * @property bool confirmed
 * @property string user_login
 * @property string currency
 * @property string payment_method
 * @property bool payment_method_cod
 * @property float payment_done
 * @property string user_comments
 * @property string admin_comments
 * @property string email
 * @property string phone
 * @property string delivery_method
 * @property float delivery_price
 * @property string delivery_package_module
 * @property string delivery_package_nr
 * @property string delivery_fullname
 * @property string delivery_company
 * @property string delivery_address
 * @property string delivery_postcode
 * @property string delivery_city
 * @property string delivery_country
 * @property string delivery_country_code
 * @property string delivery_point_id
 * @property string delivery_point_name
 * @property string delivery_point_address
 * @property string delivery_point_postcode
 * @property string delivery_point_city
 * @property string invoice_fullname
 * @property string invoice_company
 * @property string invoice_nip
 * @property string invoice_address
 * @property string invoice_postcode
 * @property string invoice_city
 * @property string invoice_country
 * @property string invoice_country_code
 * @property bool want_invoice
 * @property string extra_field_1
 * @property string extra_field_2
 * @property string order_page
 * @property int pick_state
 * @property int pack_state
 * @property OrderProductScheme[]|Collection products
 *
 * Class OrderScheme
 * @package Eurofirany\BaselinkerConnector\Responses\Schemes
 */
class OrderScheme extends CanCast {
    protected array $map = ['products' => OrderProductScheme::class];
}