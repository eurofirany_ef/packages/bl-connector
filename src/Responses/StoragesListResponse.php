<?php

namespace Eurofirany\BaselinkerConnector\Responses;

use Eurofirany\BaselinkerConnector\Responses\Schemes\StorageScheme;

/**
 * @property StorageScheme storages
 * Class StoragesListResponse
 * @package Eurofirany\BaselinkerConnector\Responses
 */
class StoragesListResponse extends BaseResponse {}