<?php

namespace Eurofirany\BaselinkerConnector\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin Builder
 * @property int id
 * @property string name
 * @property string storage_id
 * @property string token
 */
class BaselinkerChannel extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'storage_id', 'token'];
}
