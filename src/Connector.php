<?php

namespace Eurofirany\BaselinkerConnector;

use Eurofirany\BaselinkerConnector\Models\BaselinkerChannel;
use Eurofirany\BaselinkerConnector\Responses\AddProductResponse;
use Eurofirany\BaselinkerConnector\Responses\BaseResponse;
use Eurofirany\BaselinkerConnector\Responses\CreatePackageResponse;
use Eurofirany\BaselinkerConnector\Responses\OrderPackagesResponse;
use Eurofirany\BaselinkerConnector\Responses\OrdersResponse;
use Eurofirany\BaselinkerConnector\Responses\PaymentsHistoryResponse;
use Eurofirany\BaselinkerConnector\Responses\ProductsDataResponse;
use Eurofirany\BaselinkerConnector\Responses\ProductsListResponse;
use Eurofirany\BaselinkerConnector\Responses\StoragesListResponse;
use Eurofirany\BaselinkerConnector\Responses\UpdateStockOrPriceResponse;
use GuzzleHttp\Exception\GuzzleException;

class Connector
{
    /** @var Api */
    private Api $api;

    /**
     * Connector constructor.
     * @param BaselinkerChannel $channel
     */
    public function __construct(BaselinkerChannel $channel)
    {
        $this->api = new Api($channel);
    }

    /**
     * @param array $params
     * @return OrdersResponse
     * @throws GuzzleException
     */
    public function getOrders(array $params): OrdersResponse
    {
        return OrdersResponse::cast(
            $this->api->request('getOrders', $params, 60, true)
        );
    }

    /**
     * @param array $params
     * @return ProductsListResponse
     * @throws GuzzleException
     */
    public function getProductsList(array $params = []): ProductsListResponse
    {
        return ProductsListResponse::cast(
            $this->api->request('getProductsList', $params)
        );
    }

    /**
     * @param array $productsIds
     * @return ProductsDataResponse
     * @throws GuzzleException
     */
    public function getProductsData(array $productsIds): ProductsDataResponse
    {
        return ProductsDataResponse::cast(
            $this->api->request('getProductsData', ['products' => $productsIds])
        );
    }

    /**
     * @param array $products
     * @return UpdateStockOrPriceResponse
     * @throws GuzzleException
     */
    public function updateStocks(array $products): UpdateStockOrPriceResponse
    {
        return UpdateStockOrPriceResponse::cast(
            $this->api->request('updateProductsQuantity', ['products' => $products])
        );
    }

    /**
     * @param array $products
     * @return UpdateStockOrPriceResponse
     * @throws GuzzleException
     */
    public function updatePrices(array $products): UpdateStockOrPriceResponse
    {
        return UpdateStockOrPriceResponse::cast(
            $this->api->request('updateProductsPrices', ['products' => $products])
        );
    }

    /**
     * @param array $productData
     * @return AddProductResponse
     * @throws GuzzleException
     */
    public function saveProduct(array $productData): AddProductResponse
    {
        return AddProductResponse::cast(
            $this->api->request('addProduct', $productData)
        );
    }

    /**
     * @param int $orderId
     * @return OrderPackagesResponse
     * @throws GuzzleException
     */
    public function getOrderPackages(int $orderId): OrderPackagesResponse
    {
        return OrderPackagesResponse::cast(
            $this->api->request('getOrderPackages', ['order_id' => $orderId])
        );
    }

    /**
     * @param int $orderId
     * @param string $courierCode
     * @param string $packageNumber
     * @param int $pickupDate
     * @return CreatePackageResponse
     * @throws GuzzleException
     */
    public function sendOrderPackage(
        int    $orderId,
        string $courierCode,
        string $packageNumber,
        int    $pickupDate
    ): CreatePackageResponse
    {
        return CreatePackageResponse::cast(
            $this->api->request('createPackageManual', [
                'order_id' => $orderId,
                'courier_code' => $courierCode,
                'package_number' => $packageNumber,
                'pickup_date' => $pickupDate
            ])
        );
    }

    /**
     * @param int|string $productId
     * @return BaseResponse
     * @throws GuzzleException
     */
    public function deleteProduct(int|string $productId): BaseResponse
    {
        return BaseResponse::cast(
            $this->api->request('deleteProduct', [
                'product_id' => $productId
            ])
        );
    }

    /**
     * @param int|string $orderId
     * @return PaymentsHistoryResponse
     * @throws GuzzleException
     */
    public function getOrderPaymentHistory(int|string $orderId): PaymentsHistoryResponse
    {
        return PaymentsHistoryResponse::cast(
            $this->api->request('getOrderPaymentsHistory', [
                'order_id' => $orderId
            ])
        );
    }

    /**
     * @param int|string $orderId
     * @param int $newStatusId
     * @return BaseResponse
     * @throws GuzzleException
     */
    public function updateOrderStatusId(int|string $orderId, int $newStatusId): BaseResponse
    {
        return BaseResponse::cast(
            $this->api->request('setOrderStatus', [
                'order_id' => $orderId,
                'status_id' => $newStatusId
            ])
        );
    }

    /**
     * @param int|string $orderId
     * @param int|string $orderProductId
     * @return BaseResponse
     * @throws GuzzleException
     */
    public function deleteOrderProduct(int|string $orderId, int|string $orderProductId): BaseResponse
    {
        return BaseResponse::cast(
            $this->api->request('deleteOrderProduct', [
                'order_id' => $orderId,
                'order_product_id' => $orderProductId
            ])
        );
    }

    /**
     * @param int|string $orderId
     * @param string $productId
     * @param string $name
     * @param string|float $price
     * @return BaseResponse
     * @throws GuzzleException
     */
    public function addProductToOrder(
        int|string   $orderId,
        string       $productId,
        string       $name,
        string|float $price
    ): BaseResponse
    {
        return BaseResponse::cast(
            $this->api->request('addOrderProduct', [
                'order_id' => $orderId,
                'product_id' => $productId,
                'name' => $name,
                'price' => $price,
                'storage' => 'db',
                'tax_rate' => 23
            ])
        );
    }

    /**
     * @return StoragesListResponse
     * @throws GuzzleException
     */
    public function test(): StoragesListResponse
    {
        return StoragesListResponse::cast(
            $this->api->request('getStoragesList')
        );
    }

    /**
     * Set if you want or not to ignore queue for next requests
     * @param bool $ignoreQueueForRequests
     */
    public function setIgnoreQueueForRequests(bool $ignoreQueueForRequests)
    {
        $this->api->setIgnoreQueueForRequests($ignoreQueueForRequests);
    }
}
