<?php

namespace Eurofirany\BaselinkerConnector\Console\Commands;

use Exception;

class DeleteBaselinkerChannelCommand extends BaselinkerChannelDataCommand
{
    protected $signature = 'baselinker:channel:delete {channel_id?}';

    protected $description = 'Delete baselinkerChannel';

    /**
     * @throws Exception
     */
    public function handle()
    {
        $this->getChannel();

        $this->baselinkerChannelRepository->delete($this->channel);
    }
}
