<?php

namespace Eurofirany\BaselinkerConnector\Console\Commands;

class UpdateBaselinkerChannelCommand extends BaselinkerChannelDataCommand
{
    protected $signature = 'baselinker:channel:update {channel_id?}';

    protected $description = 'Update allegro account data';

    public function handle()
    {
        $this->getDataFromUser(true);

        $this->baselinkerChannelRepository->update($this->channel, [
            'name' => $this->channelName,
            'storage_id' => $this->channelStorageId,
            'token' => $this->channelToken
        ]);
    }
}
