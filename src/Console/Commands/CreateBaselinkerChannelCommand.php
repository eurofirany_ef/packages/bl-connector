<?php

namespace Eurofirany\BaselinkerConnector\Console\Commands;

class CreateBaselinkerChannelCommand extends BaselinkerChannelDataCommand
{
    protected $signature = 'baselinker:channel:create';
    protected $description = 'Create new baselinker channel';

    public function handle()
    {
        $this->getDataFromUser();

        $this->baselinkerChannelRepository->store([
           'name' => $this->channelName,
           'storage_id' => $this->channelStorageId,
           'token' => $this->channelToken
        ]);
    }
}
