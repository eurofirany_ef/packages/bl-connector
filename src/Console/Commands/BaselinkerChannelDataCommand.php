<?php

namespace Eurofirany\BaselinkerConnector\Console\Commands;

use Eurofirany\BaselinkerConnector\Models\BaselinkerChannel;
use Eurofirany\BaselinkerConnector\Repositories\BaselinkerChannelRepository;
use Illuminate\Console\Command;

class BaselinkerChannelDataCommand extends Command
{
    protected BaselinkerChannelRepository $baselinkerChannelRepository;

    protected string $channelName;
    protected string $channelStorageId;
    protected string $channelToken;
    protected null|BaselinkerChannel $channel = null;

    public function __construct(BaselinkerChannelRepository $baselinkerChannelRepository)
    {
        parent::__construct();
        $this->baselinkerChannelRepository = $baselinkerChannelRepository;
    }

    protected function getDataFromUser(bool $withChannel = false)
    {
        if ($withChannel)
            $this->getChannel();

        $this->channelName = $this->askForParameter('name');
        $this->channelStorageId = $this->askForParameter('storage_id');
        $this->channelToken = $this->askForParameter('token');
    }

    protected function getChannel()
    {
        $channelId = $this->argument('channel_id');

        if ($channelId === null) {
            $options = $this->baselinkerChannelRepository->index()
                ->map(fn(BaselinkerChannel $baselinkerChannel) => sprintf(
                    "%s: %s",
                    $baselinkerChannel->id,
                    $baselinkerChannel->name
                ));

            if($options->count() > 0) {
                $option = $this->choice(
                    'Select baselinker channel: ',
                    $options->toArray()
                );

                $channelId = current(explode(':', $option));
            } else {
                $this->info('No channels found, create once first');
                die();
            }
        }

        $this->channel = $this->baselinkerChannelRepository->find($channelId);
    }

    private function askForParameter(string $parameter)
    {
        return $this->ask(
            $this->text($parameter),
            $this->channel?->{$parameter}
        );
    }

    private function text(string $parameter): string
    {
        return sprintf(
            'Channel %s%s:',
            $parameter,
            $this->channel ? sprintf(" (%s)", $this->channel->{$parameter}) : ''
        );
    }
}