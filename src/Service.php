<?php

namespace Eurofirany\BaselinkerConnector;

use Eurofirany\BaselinkerConnector\Models\BaselinkerChannel;
use Eurofirany\BaselinkerConnector\Responses\AddProductResponse;
use Eurofirany\BaselinkerConnector\Responses\BaseResponse;
use Eurofirany\BaselinkerConnector\Responses\CreatePackageResponse;
use Eurofirany\BaselinkerConnector\Responses\OrderPackagesResponse;
use Eurofirany\BaselinkerConnector\Responses\Schemes\OrderScheme;
use Eurofirany\BaselinkerConnector\Responses\Schemes\PaymentScheme;
use Eurofirany\BaselinkerConnector\Responses\Schemes\ProductDataScheme;
use Eurofirany\BaselinkerConnector\Responses\Schemes\ProductScheme;
use Eurofirany\BaselinkerConnector\Responses\StoragesListResponse;
use Eurofirany\BaselinkerConnector\Responses\UpdateStockOrPriceResponse;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Collection;

class Service
{
    private Connector $connector;

    /**
     * Service constructor.
     * @param BaselinkerChannel $channel
     */
    public function __construct(BaselinkerChannel $channel)
    {
        $this->connector = new Connector($channel);
    }

    /**
     * Get all orders by status id
     * @param int $statusId Status ID for orders which will be taken
     * @return OrderScheme[]|Collection
     * @throws GuzzleException
     */
    public function getOrdersByStatusId(int $statusId): array|Collection
    {
        $params = ['status_id' => $statusId];

        do {
            if (isset($lastOrder))
                $params['date_confirmed_from'] = $lastOrder->date_confirmed + 1;

            $nextOrders = $this->connector->getOrders($params)->orders ?? collect([]);

            $lastOrder = $nextOrders->last();
            $orders = array_merge($orders ?? [], $nextOrders->toArray());
        } while ($nextOrders->count() > 0);

        return collect($orders);
    }

    /**
     * Get orders by orderID
     * @param int $orderId Order ID for which orders will be taken
     * @return OrderScheme[]|Collection
     * @throws GuzzleException
     */
    public function getOrdersByOrderId(int $orderId): Collection|array
    {
        return $this->connector->getOrders(['order_id' => $orderId, 'get_unconfirmed_orders' => true])->orders;
    }

    /**
     * Get products with data from Baselinker
     * @param string|array|null $productSku SKU of product to be taken or array of products sku, if null get all
     * @return Collection|ProductDataScheme[]
     * @throws GuzzleException
     */
    public function getProducts(string|array|null $productSku = null): Collection|array
    {
        $productsIdCollections = $this->getProductsList($productSku)->pluck('product_id')->chunk(1000);

        foreach ($productsIdCollections as $productsIds)
            $products = array_merge(
                $products ?? [],
                $this->connector->getProductsData($productsIds->toArray())->products->toArray()
            );

        return collect($products ?? []);
    }

    /**
     * Get products list (base data without details)
     * @param string|array|null $productSku SKU of product to be taken or array of products sku, if null get all
     * @return Collection|ProductScheme[]
     * @throws GuzzleException
     */
    public function getProductsList(string|array|null $productSku = null): Collection|array
    {
        $allProducts = [];
        $priceFrom = 0;

        if (is_array($productSku)) {
            foreach ($productSku as $sku) {
                $products = $this->connector->getProductsList([
                    'filter_sku' => $sku
                ])->products->toArray();

                $allProducts = array_merge($allProducts, $products);
            }
        } else {
            do {
                if ($productSku)
                    $products = $this->connector->getProductsList([
                        'filter_sku' => $productSku
                    ])->products;
                else
                    $products = $this->connector->getProductsList([
                        'filter_sort' => 'price ASC',
                        'filter_price_from' => $priceFrom
                    ])->products;

                $allProducts = array_merge($allProducts, $products->toArray());

                /** @var ProductScheme $lastProduct */
                $lastProduct = $products->last();

                $priceFrom = $lastProduct->price_brutto ?? 0;
            } while ($products->count() >= 49999);
        }

        return collect($allProducts);
    }

    /**
     * Get all orders from year and month
     * @param int $year Year for which orders will be taken
     * @param int $month Month for which orders will be taken
     * @return OrderScheme[]|Collection
     * @throws GuzzleException
     */
    public function getOrdersFromYearAndMonth(int $year, int $month): array|Collection
    {
        $dateConfirmedFrom = strtotime(date('Y-m-d', strtotime(sprintf("%d-%d-01", $year, $month))));

        do {
            $orders = $this->connector->getOrders(['date_confirmed_from' => $dateConfirmedFrom])->orders;

            /** @var OrderScheme[] $allOrders */
            $allOrders = array_merge($allOrders ?? [], $orders->toArray());

            if (count($allOrders) > 0)
                $dateConfirmedFrom = end($allOrders)->date_confirmed;
        } while ($orders->count() === 100);

        return collect($allOrders);
    }

    /**
     * Update stocks on Baselinker
     * @param Collection $stocks Stocks data to update
     * @return UpdateStockOrPriceResponse[]|Collection
     * @throws GuzzleException
     */
    public function updateStocks(Collection $stocks): array|Collection
    {
        /** @var Collection $stockArray */
        foreach ($stocks->chunk(1000) as $stockArray)
            $statuses[] = $this->connector->updateStocks($stockArray->toArray());

        return collect($statuses ?? []);
    }

    /**
     * Update products prices on Baselinker
     * @param Collection $prices Prices data to update
     * @return UpdateStockOrPriceResponse[]|Collection
     * @throws GuzzleException
     */
    public function updatePrices(Collection $prices): array|Collection
    {
        /** @var Collection $pricesArray */
        foreach ($prices->chunk(1000) as $pricesArray)
            $statuses[] = $this->connector->updatePrices($pricesArray->toArray());

        return collect($statuses ?? []);
    }

    /**
     * Save new product on Baselinker
     * @param array $data Product data to be saved
     * @return AddProductResponse
     * @throws GuzzleException
     */
    public function saveProduct(array $data): AddProductResponse
    {
        return $this->connector->saveProduct($data);
    }

    /**
     * Get all order packages
     * @param int $orderId Order ID for which packages will be taken
     * @return OrderPackagesResponse
     * @throws GuzzleException
     */
    public function getOrderPackages(int $orderId): OrderPackagesResponse
    {
        return $this->connector->getOrderPackages($orderId);
    }

    /**
     * Add new order package to order
     * @param int $orderId Order ID for which package will be added
     * @param string $courierCode Code of courier by which order will be realized
     * @param string $packageNumber Number of package
     * @param int $pickupDate Date of package pickup
     * @return CreatePackageResponse
     * @throws GuzzleException
     */
    public function sendOrderPackage(
        int    $orderId,
        string $courierCode,
        string $packageNumber,
        int    $pickupDate
    ): CreatePackageResponse
    {
        return $this->connector->sendOrderPackage($orderId, $courierCode, $packageNumber, $pickupDate);
    }

    /**
     * Delete product from Baselinker
     * @param int|string $productId ID of product to be deleted
     * @return BaseResponse
     * @throws GuzzleException
     */
    public function deleteProduct(int|string $productId): BaseResponse
    {
        return $this->connector->deleteProduct($productId);
    }

    /**
     * Get order payment history from BL
     * @param int|string $orderId Order ID for which history will be taken
     * @return PaymentScheme[]|Collection
     * @throws GuzzleException
     */
    public function getOrderPaymentHistory(int|string $orderId): array|Collection
    {
        return $this->connector->getOrderPaymentHistory($orderId)->payments;
    }

    /**
     * Change order status ID
     * @param int|string $orderId Order ID which will be updated
     * @param int $newStatusId New status ID
     * @return BaseResponse
     * @throws GuzzleException
     */
    public function updateOrderStatus(
        int|string $orderId,
        int        $newStatusId
    ): BaseResponse
    {
        return $this->connector->updateOrderStatusId($orderId, $newStatusId);
    }

    /**
     * Delete product from order
     * @param int|string $orderId Order from which product will be deleted
     * @param int|string $orderProductId Order Product ID that will be deleted
     * @return BaseResponse
     * @throws GuzzleException
     */
    public function deleteOrderProduct(
        int|string $orderId,
        int|string $orderProductId
    ): BaseResponse
    {
        return $this->connector->deleteOrderProduct($orderId, $orderProductId);
    }

    /**
     * Add new product to order
     * @param int|string $orderId Order ID for which product will be added
     * @param string $productId Produch ID which will be added to order
     * @param string $name Product name in order
     * @param string|float $price Product price in order
     * @return BaseResponse
     * @throws GuzzleException
     */
    public function addProductToOrder(
        int|string   $orderId,
        string       $productId,
        string       $name,
        string|float $price
    ): BaseResponse
    {
        return $this->connector->addProductToOrder(
            $orderId,
            $productId,
            $name,
            $price
        );
    }

    /**
     * Test connector
     * @return StoragesListResponse
     * @throws GuzzleException
     */
    public function test(): StoragesListResponse
    {
        return $this->connector->test();
    }

    /**
     * Set if you want or not to ignore queue for next requests
     * @param bool $ignoreQueueForRequests
     */
    public function setIgnoreQueueForRequests(bool $ignoreQueueForRequests)
    {
        $this->connector->setIgnoreQueueForRequests($ignoreQueueForRequests);
    }
}
