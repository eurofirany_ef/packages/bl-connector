<?php

namespace Eurofirany\BaselinkerConnector\Providers;

use Eurofirany\BaselinkerConnector\Console\Commands\CreateBaselinkerChannelCommand;
use Eurofirany\BaselinkerConnector\Console\Commands\DeleteBaselinkerChannelCommand;
use Eurofirany\BaselinkerConnector\Console\Commands\UpdateBaselinkerChannelCommand;
use Illuminate\Support\ServiceProvider;

class BaselinkerConnectorServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/bl.php', 'bl_config');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');

        $this->publishes([
            __DIR__.'/../config/bl.php' => app()->configPath('bl.php'),
        ], 'bl_config');

        if ($this->app->runningInConsole()) {
            $this->commands([
                CreateBaselinkerChannelCommand::class,
                UpdateBaselinkerChannelCommand::class,
                DeleteBaselinkerChannelCommand::class
            ]);
        }
    }
}
