# Docs for PHP 8 Version

## Requirements

* Laravel ^8.0
* PHP ^8.0

## Install

First install package with composer:

    composer require eurofirany/bl-connector

Migrate tables for queue
    
    php artisan migrate

Next public config file to your app config folder:

    php artisan vendor:publish --tag=bl_config

This generates **bl.php** file in **config** folder

    /** Queue Settings */
    'queue' => [
        'enabled' => false, // Is queue for connector enabled
        'max_tries' => 3, // Max tries for failed job
        'max_running_jobs' => 5, // Max running jobs at the same time
        'max_wait_seconds' => 300, // Wait seconds when there are too many jobs
        'sleep_seconds' => 10 // Seconds to wait between attempts
    ]


## Lumen Framework

Put this lines in **bootstrap/app.php** file:

    $app->register(\Eurofirany\BaselinkerConnector\Providers\BaselinkerConnectorServiceProvider::class);
    $app->register(\Eurofirany\Microservices\Providers\MicroservicesConnectorServiceProvider::class);

    $app->configure('bl');

For publish vendor config install **laravelista/lumen-vendor-publish** package
or just create **bl.php** file in **config** folder.

## Commands

name|description
---|---
|baselinker:channel:create|Create new Baselinker Channel in DB
|baselinker:channel:update {channel_id?}|Update Channel by give in or if empty select form list
|baselinker:channel:delete {channel_id?}|Delete Channel by give in or if empty select form list